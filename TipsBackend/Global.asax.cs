﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using TipsBackend.Models.Daos;
using TipsBackend.Models.Daos.Implementations;
using TipsBackend.Models.UnitOfWork;

namespace TipsBackend
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            // *** Order is important *** //

            InitDependencies();

            ConfigureWebApi();
        }

        /// <summary>
        ///     Usual Web API configuration stuff.
        /// </summary>
        private void ConfigureWebApi()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            // Decorate all controllers with unit of work
            GlobalConfiguration.Configuration.Filters.Add(new UnitOfWorkAttribute());
        }

        /// <summary>
        ///     A method for injecting dependencies
        /// </summary>
        private void InitDependencies()
        {
            // Create the container as usual.
            var container = new Container();
            container.Options.PropertySelectionBehavior = new DependencyAttributeSelectionBehaviour();

            // Register your types, for instance using the RegisterWebApiRequest
            // extension from the integration package:
            container.RegisterWebApiRequest<IUnitOfWork, NHibernateUnitOfWork>();
            container.RegisterSingle<ISessionSource, NHibernateSessionSource>();
            container.RegisterSingle<IAppSettings, AppSettings>();


            container.Register(() =>
            {
                var uow = (INHibernateUnitOfWork) (container.GetInstance<IUnitOfWork>());
                return uow.Session;
            });



            container.Register<IUserRepository, UserRepository>();

            // This is an extension method from the integration package.
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            container.Verify();


            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}