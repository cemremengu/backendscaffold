﻿using System.Collections.Generic;
using TipsBackend.Models.Daos;
using TipsBackend.Models.Entities;

namespace TipsBackend.Services
{
    public class UserService
    {
        public IUserRepository UserRepository { private get; set; }

        public virtual IList<User> GetAllUsers()
        {
            return UserRepository.GetAll();
        }

        public virtual User GetUser(int id)
        {
            return UserRepository.Get(id);
        }

        public virtual void Save(User user)
        {
            UserRepository.Save(user);
        }

        public virtual void DeleteUser(User user)
        {
            UserRepository.Delete(user);
        }

        public virtual void UpdateUser(User user)
        {
            UserRepository.Update(user);
        }
    }
}