﻿using System.Collections.Generic;
using System.Web.Http;

namespace TipsBackend.Controllers
{
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {
        [Route]
        public IEnumerable<string> Get()
        {
            return new[] {"value1", "value2"};
        }

        [Route("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}