﻿namespace TipsBackend
{
    public interface IAppSettings
    {
        string GetSetting(string settingName);
    }
}
