﻿using System;

namespace TipsBackend
{
    public class AppSettings: IAppSettings
    {
        public string GetSetting(string settingName)
        {
            return System.Configuration.ConfigurationManager.AppSettings[settingName];
        }
    }
}