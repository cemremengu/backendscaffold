﻿using TipsBackend.Models.Entities;

namespace TipsBackend.Models.Daos
{
    public interface IUserRepository : IGenericDao<User, int>
    {
    }
}
