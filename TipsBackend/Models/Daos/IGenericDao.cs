﻿using System.Collections.Generic;

namespace TipsBackend.Models.Daos
{
    public interface IGenericDao<TEntity, TId>
    {
        TEntity Get(TId id);
        void Save(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        IList<TEntity> GetAll();
    }
}
