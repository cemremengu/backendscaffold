﻿using System.Collections.Generic;
using NHibernate;

namespace TipsBackend.Models.Daos.Implementations
{
    public class GenericHibernateDao<TEntity, TId> : IGenericDao<TEntity, TId> 
    {
        public ISessionFactory SessionFactory { private get; set; }

        protected ISession CurrentSession
        {
            get
            {
                return SessionFactory.GetCurrentSession();
            }
        }

        public virtual TEntity Get(TId id)
        {
            return CurrentSession.Get<TEntity>(id);
        }

        public virtual void Save(TEntity entity)
        {
            CurrentSession.Save(entity);
        }

        public virtual void Update(TEntity entity)
        {
            CurrentSession.Update(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            CurrentSession.Delete(entity);
        }

        public virtual IList<TEntity> GetAll()
        {
            return CurrentSession.CreateCriteria(typeof(TEntity))
                        .List<TEntity>();
        }
    }
}