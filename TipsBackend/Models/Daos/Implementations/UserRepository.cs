﻿using TipsBackend.Models.Entities;

namespace TipsBackend.Models.Daos.Implementations
{
    public class UserRepository : GenericHibernateDao<User, int>, IUserRepository
    {
    }
}