﻿using System;
using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;

namespace TipsBackend.Models.UnitOfWork
{
    public interface ISessionSource
    {
        ISession CreateSession();
        void BuildSchema();
    }

    public class NHibernateSessionSource : ISessionSource
    {
        private const string CONNECTION_STRING_NAME = "Main.ConnectionString";
        private static readonly object factorySyncRoot = new object();

        private readonly IAppSettings appSettings;
        private readonly ISessionFactory sessionFactory;
        private readonly Configuration configuration;

        public NHibernateSessionSource(IAppSettings appSettings)
        {
            if (sessionFactory != null) return;

            lock (factorySyncRoot)
            {
                if (sessionFactory != null) return;

                this.appSettings = appSettings;
                configuration = AssembleConfiguration();
                sessionFactory = configuration.BuildSessionFactory();
            }
        }

        private Configuration AssembleConfiguration()
        {
            var connectionString = appSettings.GetSetting(CONNECTION_STRING_NAME);

            return Fluently.Configure()
                    .Database(OracleClientConfiguration.Oracle10
                        .ConnectionString(connectionString).AdoNetBatchSize(50))
                    .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()))
                    .BuildConfiguration();
        }

        public ISession CreateSession()
        {
            return sessionFactory.OpenSession();
        }


        public void BuildSchema()
        {
            throw new NotImplementedException();
        }
    }
}