﻿using System;
using System.Linq;
using System.Reflection;
using SimpleInjector.Advanced;

namespace TipsBackend.Models.UnitOfWork
{
    public class DependencyAttributeSelectionBehaviour : IPropertySelectionBehavior
    {
        public bool SelectProperty(Type type, PropertyInfo prop)
        {
            return prop.GetCustomAttributes(typeof(Dependency)).Any();
        }
    }
}