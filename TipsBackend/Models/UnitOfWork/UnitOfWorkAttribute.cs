﻿using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace TipsBackend.Models.UnitOfWork
{
    public class UnitOfWorkAttribute : ActionFilterAttribute
    {
        [Dependency]
        public IUnitOfWork UnitOfWorkFactory { get; set; }

        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            UnitOfWorkFactory.Begin();
        }

        // TODO: Fix this: We should also probably check if exception is handled
        public override void OnActionExecuted(HttpActionExecutedContext filterContext)
        {
            var uow = UnitOfWorkFactory;

            try
            {
                if (filterContext.Exception != null)
                {
                    uow.Rollback();
                }
                else
                {
                    uow.Commit();
                }
            }
            catch
            {
                uow.Rollback();
                throw;
            }
            finally
            {
                uow.Dispose();
            }
        }
    }
}