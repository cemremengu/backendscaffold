﻿using System;
using NHibernate;

namespace TipsBackend.Models.UnitOfWork
{
    internal interface INHibernateUnitOfWork : IUnitOfWork
    {
        ISession Session { get; }
    }

    public class NHibernateUnitOfWork : INHibernateUnitOfWork
    {
        private readonly ISessionSource sessionSource;
        private ITransaction transaction;
        private ISession session;

        private bool disposed;
        private bool begun;

        public NHibernateUnitOfWork(ISessionSource sessionSource)
        {
            this.sessionSource = sessionSource;
            Begin();
        }

        private static readonly object @lock = new object();

        public ISession Session
        {
            get
            {
                if (session == null)
                {
                    lock (@lock)
                    {
                        if (session == null)
                        {
                            session = sessionSource.CreateSession();
                        }
                    }
                }

                return session;
            }
        }

        public void Begin()
        {
            CheckIsDisposed();

            if (begun)
            {
                return;
            }

            if (transaction != null)
            {
                transaction.Dispose();
            }

            transaction = Session.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);

            begun = true;
        }

        public void Commit()
        {
            CheckIsDisposed();
            CheckHasBegun();

            if (transaction.IsActive && !transaction.WasRolledBack)
            {
                transaction.Commit();
            }

            begun = false;
        }

        public void Rollback()
        {
            CheckIsDisposed();
            CheckHasBegun();

            if (transaction.IsActive)
            {
                transaction.Rollback();
                Session.Clear();
            }

            begun = false;
        }

        public void Dispose()
        {
            if (!begun || disposed)
                return;

            if (transaction != null)
            {
                transaction.Dispose();
            }

            if (Session != null)
            {
                Session.Dispose();
            }

            disposed = true;
        }

        private void CheckHasBegun()
        {
            if (!begun)
            {
                throw new InvalidOperationException("Must call Begin() on the unit of work before committing");
            }
        }

        private void CheckIsDisposed()
        {
            if (disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }
    }
}