﻿using FluentNHibernate.Mapping;
using TipsBackend.Models.Entities;

namespace TipsBackend.Models.Mappings
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table("USERS");
            OptimisticLock.None();


            Id(x => x.Id)
                .Column("ID")
                .GeneratedBy.Native("SEQ_USERS");
            Map(x => x.Active).Column("ACTIVE");
            Map(x => x.ComputerName).Column("COMPUTER_NAME");
            Map(x => x.LastSessionTime).Column("LAST_SESSION_TIME");
            Map(x => x.IsAdmin).Column("IS_ADMIN");
            Map(x => x.Username).Column("USERNAME");
            Map(x => x.Userpass).Column("USERPASS");
            Map(x => x.SkinType).Column("SKIN_TYPE");
            Map(x => x.Email).Column("EMAIL");


        }
    }
}