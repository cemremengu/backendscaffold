﻿using System;
using System.Collections.Generic;

namespace TipsBackend.Models.Entities
{
    public class User
    {
 

        public virtual bool Active { get; set; }

        public virtual string ComputerName { get; set; }

        public virtual int Id { get; protected set; }

        public virtual DateTime? LastSessionTime { get; set; }

        public virtual bool IsAdmin { get; set; }

        public virtual string Username { get; set; }

        public virtual string Userpass { get; set; }

        public virtual int SkinType { get; set; }

        public virtual string Email { get; set; }



        public override int GetHashCode()
        {
            int toReturn = base.GetHashCode();
            toReturn ^= Id.GetHashCode();
            return toReturn;
        }


        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            User toCompareWith = obj as User;
            return toCompareWith != null && ((Id == toCompareWith.Id));
        }
    }
}